<form action="resetpassword.php" method="post">
    <fieldset>
        <legend>Change Password</legend>
        <div class="form-group">
            <input class="form-control" name="oldpassword" placeholder="Old Password" type="password"/>
        </div>
        <div class="form-group">
            <input autocomplete="off" autofocus class="form-control" name="newpassword" placeholder="New password" type="password"/>
        </div>
        <div class="form-group">
            <input autocomplete="off" autofocus class="form-control" name="renewpassword" placeholder="Retype New password" type="password"/>
        </div>
        <div class="form-group">
            <button class="btn btn-default" type="submit">
                <span aria-hidden="true" class="glyphicon glyphicon-log-in"></span>
                Reset password
            </button>
        </div>
    </fieldset>
</form>
