<?php

function render($view, $values = [])
{

    if (file_exists("../views/$view"))
    {
        // extract variables into local scope
        extract($values);

        // render header
        require("../views/header.php");

        // render view
        require("../views/$view");

        // render footer
        require("../views/footer.php");
    }

    // else err
    else
    {
        trigger_error("Invalid view: $view", E_USER_ERROR);
    }
}

/**
 * Apologizes to user with message.
 */
function apologize($message)
{
   
    render("apology.php", ["message" => $message]);
   

}

function query($query)
{
  $config = include('../config.php');

  // Try to connect
  $con = mysqli_connect($config['mysql']['host'],$config['mysql']['username'],$config['mysql']['password'],$config['mysql']['database']);
  if (mysqli_connect_errno())
  {
    echo "Failed to connect to MySQL: " . mysqli_connect_error();
    die;
  }

  // Perform query
  $result = mysqli_query($con, $query);

  if (is_object($result))
  {
    $rows = [];
    while($row = $result->fetch_assoc()) {
        $rows[] = $row;
    }
    return $rows;
  }

  else
  {
    return $result;
  }

  mysqli_close($con);
}

function redirect($location)
{
    if (headers_sent($file, $line))
    {
        trigger_error("HTTP headers already sent at {$file}:{$line}", E_USER_ERROR);
    }
    header("Location: {$location}");
    exit;
}

 ?>
