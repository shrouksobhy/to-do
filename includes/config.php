<?php
require("../config.php");

require_once('helper.php');
// display errors, warnings, and notices
session_start();
//require authentication for all pages except /login.php, /logout.php, and /register.php
 if (!in_array($_SERVER["PHP_SELF"], ["/login.php", "/logout.php", "/register.php"]))
 {
     if (empty($_SESSION["id"]))
     {
         redirect("login.php");
     }
 }
 if (isset($_GET['logout'])) {
  	session_destroy();
  	unset($_SESSION['username']);
  	header("location: login.php");
  }


ini_set("display_errors", true);
error_reporting(E_ALL ^ E_NOTICE);






 ?>