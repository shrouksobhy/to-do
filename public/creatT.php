<?php

    // configuration
    require("../includes/config.php");
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
     render("creatT.php", ["title" => "task"]);   
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["tname"]))
        {
            apologize("You must valid task name.");
        }
        else if (empty($_POST["cname"]))
        {
            apologize("You must enter the category name.");
        }
        else {

          $tname = $_POST['tname'];
          // query database for user
          $rows = query("INSERT INTO  tasks(tname) VALUES ('$tname') ");

          $categories = query("SELECT * FROM categories");

        render("categories.php", ["title" => "Categories", "categories" => $categories]);

          }
    }  

?>
