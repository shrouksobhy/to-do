<?php
    require("../includes/config.php");


    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {

        render("register.php", ["title" => "Register"]);
    }

    
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["username"]))
        {
            apologize("You must enter a username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must enter a password.");
        }
        else if (empty($_POST["confirmation"]))
        {
            apologize("You must confirm your password.");
        }
        else if ($_POST['password'] != $_POST['confirmation']) {
            apologize("Your password and password confirmation do not match.");
        }
        else
        {
            $username = $_POST["username"];
            $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
            $result = query("INSERT  INTO users (username, hash) VALUES('$username', '$hash')");
            

             $_SESSION['username']=$username;
             $_SESSION['hash']=$hash;

            // $_SESSION['sucesss']="you are logged in";

              

             $row = query("SELECT id FROM users ORDER BY id DESC LIMIT 1;");
             $id = $row[0]['id'];
             $_SESSION["id"] = $id;
            // redirect("home.php");  not working

            $categories = query("SELECT * FROM categories");
            render("categories.php", ["title" => "Categories", "categories" => $categories]);
           
        }

    }
?>
