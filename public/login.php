<?php

    require("../includes/config.php");



    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
        render("login.php", ["title" => "Log In"]);
    }

   else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        // validate submission
        if (empty($_POST["username"]))
        {
            apologize("You must provide your username.");
        }
        else if (empty($_POST["password"]))
        {
            apologize("You must provide your password.");
        }
        else {
          $username = $_POST['username'];
          $hash = password_hash($_POST["password"], PASSWORD_DEFAULT);
          // query database for user
          $rows = query("SELECT * FROM users WHERE username = '$username' and hash = '$hash'");

          // if we found user, check password
          if (count($rows) == 1)
          {
              // first (and only) row
              $row = $rows[0];

              // compare hash of user's input against hash that's in database
              if (password_verify($_POST["password"], $row["hash"]))
              {
                  // remember that user's now logged in by storing user's ID in session
                  $_SESSION["id"] = $row["id"];


                  // redirect to portfolio
                  //redirect("/home.php");     dosn't work

                  $categories = query("SELECT * FROM categories");
                   render("categories.php", ["title" => "Categories", "categories" => $categories]);
                   
              }
          }else{
          // else apologize
          apologize("Invalid username and/or password.");
        }
     }
  }

?>
