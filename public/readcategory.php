<?php  require("../views/header.php");  require("../includes/config.php"); ?>
<table class="infotable">

  <?php foreach ($categories as $category): ?>
<tr>
  
    <td> 
      <a href="/category.php?categoryId=<?= $category['id'] ?>">
      <?= $category['cname'] ?>
      
    </td>
</tr>
  <?php endforeach ?>
</table>

<br>

<table class="infotable">

<?php foreach ($tasks as $task): ?>

    <tr>
        <td>
          <a href="/task.php?taskId=<?= $task['id'] ?>">
            <?= $task["tname"] ?>
          </a>
        </td>

        <td><?= $task["tdescription"] ?></td>
        <td><?= $task["state"] ?></td>
        
        <div class="CRUD">
        <td>
          <a href="/readTask.php">Read Task</a>
          <a href="/creatTask.php">Creat Task</a>
          <a href="/updateTask.php">Update Task</a>
          <a href="/deleteTask.php">Delete Task</a>
        </td>
      </div>
    </tr>
<?php endforeach ?>
</table>

<a href="logout.php" id="logout">Log out</a>