<?php

    // configuration
    require("../includes/config.php");
    require("../config.php");

    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
     render("updateC.php", ["title" => "Update category "]);   
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["oldcname"]) || empty($_POST['newcname']))
        {
            apologize("wrong in old/ new category name ");
        }

        else {

          $oldcname = $_POST['oldcname'];
          $newcname=$_POST['newcname'];
          $id=$_POST['id'];
          // query database for user
          $rows = query("UPDATE categories SET cname = $newcname WHERE id = $id");

          $categories = query("SELECT * FROM categories");

          render("categories.php", ["title" => "Categories", "categories" => $categories]);
            }
          
    }
    
?>
