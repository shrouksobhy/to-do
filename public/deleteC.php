<?php

    // configuration
    require("../includes/config.php");
    require("../config.php");
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
     render("deleteC.php", ["title" => "delete category "]);   
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["cname"]))
        {
            apologize("You must write the category name.");
        }
        else {

          $cname = $_POST['cname'];

         // $id=$_POST['id']
          // query database for user
          $rows = query("DELETE FROM categories WHERE cname='$cname'");
          
           $categories = query("SELECT * FROM categories");

        render("categories.php", ["title" => "Categories", "categories" => $categories]);

          }

         
        
    }

?>
