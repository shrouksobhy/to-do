<?php

    // configuration
    require("../includes/config.php");
    // if user reached page via GET (as by clicking a link or via redirect)
    if ($_SERVER["REQUEST_METHOD"] == "GET")
    {
     render("creatC.php", ["title" => "category"]);   
    }

    // else if user reached page via POST (as by submitting a form via POST)
    else if ($_SERVER["REQUEST_METHOD"] == "POST")
    {
        if (empty($_POST["cname"]))
        {
            apologize("You must valid category name.");
        }
        else {

          $cname = $_POST['cname'];
          // query database for user
          $rows = query("INSERT INTO  categories(cname) VALUES ('$cname') ");

          $categories = query("SELECT * FROM categories");

        render("categories.php", ["title" => "Categories", "categories" => $categories]);

          }
    }  

?>
